package History.Hackathon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.demo.charts.ExampleChart;
import org.knowm.xchart.style.Styler.LegendPosition;

import History.Hackathon.model.People;

/**
 * Basic Bar Chart
 * 
 * Demonstrates the following:
 * <ul>
 * <li>Integer categories as List
 * <li>All positive values
 * <li>Single series
 * <li>Place legend at Inside-NW position
 * <li>Bar Chart Annotations
 */
public class BarChart01 implements ExampleChart<CategoryChart> {

	public static void main(String[] args) {

		ExampleChart<CategoryChart> exampleChart = new BarChart01();
		CategoryChart chart = exampleChart.getChart();
		new SwingWrapper<CategoryChart>(chart).displayChart();
	}

	@Override
  public CategoryChart getChart() {
	  
	Inputter inputter = new Inputter();
	ArrayList<People> people = inputter.getSpeaker();
	Map<Integer,Integer>ageMap=new HashMap<Integer, Integer>();
	int total=0;
	for(People p:people) {
		if(StringUtils.isNumeric(p.getAge())) {
			total++;
			int age = Integer.parseInt(p.getAge());
			if(ageMap.containsKey(age)){
				int amount= ageMap.get(age)+1;
				ageMap.put(age, amount);
				
			}else {
				ageMap.put(age, 1);
			}
		}
		
	}
	System.out.print(total);
	
	Set set = ageMap.keySet();
	Collection<Integer> val = ageMap.values();
	List<Integer> list = new ArrayList<>(val);
	List<Integer> list2 = new ArrayList<>(set);

	// Create Chart
    CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title("Score Histogram").xAxisTitle("Score").yAxisTitle("Number").build();
 
    // Customize Chart
    chart.getStyler().setLegendPosition(LegendPosition.InsideNW);
    chart.getStyler().setHasAnnotations(true);
    
    // Series
    chart.addSeries("test", list2, list);
    return chart;
  }
}
