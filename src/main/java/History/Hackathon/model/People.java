package History.Hackathon.model;

import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 *  "id": "vtls004679559_0_14",
        "type": "sailor",
        "parent": "vtls004679559",
        "name": "thomas williams",
        "dob": "blk",
        "age": "17",
        "place_of_birth": "st davids",
        "home_address": "blk",
        "f": "blk",
        "name_of_ship": "jane",
        "ship_port": "lymington",
        "date_leaving": "1873-12-30",
        "joined_ship_date": "1873-01-01",
        "joined_at_port": "lymington",
        "capacity": "ordinary seaman",
        "date_left": "1873-06-30",
        "left_port": "at sea",
        "cause_of_leaving": "remains on board",
        "sign_with_mark": "n",
        "notes": "blk"
 * @author Philip
 *
 */
public class People {
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getPlace_of_birth() {
		return place_of_birth;
	}
	public void setPlace_of_birth(String place_of_birth) {
		this.place_of_birth = place_of_birth;
	}
	public String getHome_address() {
		return home_address;
	}
	public void setHome_address(String home_address) {
		this.home_address = home_address;
	}
	public String getF() {
		return f;
	}
	public void setF(String f) {
		this.f = f;
	}
	public String getName_of_ship() {
		return name_of_ship;
	}
	public void setName_of_ship(String name_of_ship) {
		this.name_of_ship = name_of_ship;
	}
	public String getShip_port() {
		return ship_port;
	}
	public void setShip_port(String ship_port) {
		this.ship_port = ship_port;
	}
	public String getDate_leaving() {
		return date_leaving;
	}
	public void setDate_leaving(String date_leaving) {
		this.date_leaving = date_leaving;
	}
	public String getJoined_at_port() {
		return joined_at_port;
	}
	public void setJoined_at_port(String joined_at_port) {
		this.joined_at_port = joined_at_port;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getDate_left() {
		return date_left;
	}
	public void setDate_left(String date_left) {
		this.date_left = date_left;
	}
	public String getLeft_port() {
		return left_port;
	}
	public void setLeft_port(String left_port) {
		this.left_port = left_port;
	}
	public String getCause_of_leaving() {
		return cause_of_leaving;
	}
	public void setCause_of_leaving(String cause_of_leaving) {
		this.cause_of_leaving = cause_of_leaving;
	}
	public String getSign_with_mark() {
		return sign_with_mark;
	}
	public void setSign_with_mark(String sign_with_mark) {
		this.sign_with_mark = sign_with_mark;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@BsonProperty("id")
	private String id;
	private String type;
	private String name;
	private String dob;
	private String age;
	private String place_of_birth;
	private String home_address;
	private String f;
	private String name_of_ship;
	private String ship_port;
	private String date_leaving;
	private String joined_at_port;
	private String capacity;
	private String date_left;
	private String left_port;
	private String cause_of_leaving;
	private String sign_with_mark;	
	private String notes;
}
