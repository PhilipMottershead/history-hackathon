package History.Hackathon;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import History.Hackathon.model.People;

public class Main {
	
	public static String readFileAsString(String fileName) throws Exception {
		String data = "";
		data = new String(Files.readAllBytes(Paths.get(fileName)));
		return data;
	}

	public static void main(String args[]) throws Exception {
		Inputter inputter = new Inputter();
		ArrayList<People> people = inputter.getSpeaker();
		people.size();
		int sum = 0;
		for(People p :people) {
			if(p.getAge()!=null&&StringUtils.isNumeric(p.getAge())) {
				if(!p.getAge().equals("blk")) {
					int i = Integer.parseInt(p.getAge(),16);
					sum =sum + i;
				}
			}
		}
		
		System.out.print(sum/people.size()-1);
		
	}
}
