package History.Hackathon;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Splitter;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import History.Hackathon.model.People;

public class Inputter {
	
	private MongoClient mongoClient = MongoClients
			.create("mongodb://localhost:27017/test?authSource=$[authSource] --username superuser");
	
	private MongoDatabase database = mongoClient.getDatabase("test");
	
	public static File getResourceFile(String filePath) throws Exception {
		ClassLoader classLoader = new Inputter().getClass().getClassLoader();

		File file = new File(classLoader.getResource(filePath).getFile());
		return file;
	}

	public static String readFileAsString(File file) throws Exception {
		String data = "";
		data = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
		return data;
	}
	
	public void readAllFiles() throws Exception {
		List<File> files = getFilenames();
		
		for(File file: files) {
			reader(file);
		}
	}
	
	public void reader(File file) throws Exception {
		 String data = readFileAsString(file);
		 ObjectMapper objectMapper = new ObjectMapper();
		 JsonNode jsonNode = objectMapper.readValue(data, JsonNode.class);
		 String[] list = (jsonNode.toString()).split("}");
		 List<String> lists = new ArrayList<String>();
		 int index = 0;
		 for(String item: list) {
			String item2= removeFirstLetter(item); 
			StringBuilder builder = new StringBuilder();
			String string = builder.append(item2).append("}").toString();
			if(!item.equals("]")) {
				lists.add(string);
			}
			index++;
			
		 }

		 MongoCollection<Document> peoples = database.getCollection("people2");
		 List<Document> documents = new ArrayList<Document>();
		 for(String item: lists) {
			  documents.add(Document.parse(item));	
		 }
		 peoples.insertMany(documents);
		 
	}
	
	public ArrayList<People> getSpeaker(){
		CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
				fromProviders(PojoCodecProvider.builder().automatic(true).build()));
		database = database.withCodecRegistry(pojoCodecRegistry);
		
		MongoCollection<People> people = database.getCollection("people", People.class);
		ArrayList<People> peoples = people.find().into(
				new ArrayList<People>());
		people.countDocuments();
		return peoples;
	}
	
	/**
	 * Strips the last letter of the string
	 *
	 * @param string Input string
	 * @return Stripped string
	 */
	public static String removeFirstLetter(String string) {
		return string.substring(1);
	}
	
	/**
	 * Strips the last letter of the string
	 *
	 * @param string Input string
	 * @return Stripped string
	 */
	public static String removeLastLetter(String string) {
		return string.substring(0, string.length() - 1);
	}
	
	
	
	public List<File> getFilenames() throws Exception {
		List<File> results = new ArrayList<File>();
		File file = getResourceFile("data");
		File[] files = file.listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 
		
		for (File f : files) {
		    if (f.isFile()) {
		        results.add(f);
		    }
		}
		return results;
	}

}
